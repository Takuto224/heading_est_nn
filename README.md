# README #

This README would normally document whatever steps are necessary to get your application up and running.

# Example
## train

GT1205 dataset
```
python train.py --gpuid 0 --dataset gt1205 --train_list ./lists/gt1205/train.txt --val_list ./lists/gt1205/val.txt
```

RoNIN dataset
```
python train.py --gpuid 0 --dataset ronin --train_list ./lists/ronin/train.txt --val_list ./lists/ronin/val.txt
```

## test

GT1205 dataset
```
python test.py --gpuid 0 --dataset gt1205 --test_list ./lists/gt1205/test.txt
```