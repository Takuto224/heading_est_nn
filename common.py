import numpy as np
import os
import json
import copy
import math
from scipy.spatial.transform import Rotation


def save_json(path, data):
    save_path = os.path.join(path)
    with open(save_path, 'w') as f:
        json.dump(data, f, indent=2, ensure_ascii=False)

        
def read_json(path):
    path = os.path.join(path)
    with open(path, 'r') as f:
        data = json.load(f)
    return data
    

def convert_DCS_to_GCS_by_quat(vec3d, quat):
    R = Rotation.from_quat(quat)
    R_matrix = R.as_matrix()

    glob_vec3d = copy.deepcopy(vec3d)
    for i in range(len(R_matrix)):
        glob_vec3d[i, :] = np.dot(R_matrix[i], vec3d[i, :])
    return glob_vec3d


def convert_DCS_to_GCS_by_euler(vec3d, euler):
    R = Rotation.from_euler('xyz', euler)
    R_matrix = R.as_matrix()

    glob_vec3d = copy.deepcopy(vec3d)
    for i in range(len(R_matrix)):
        glob_vec3d[i, :] = np.dot(R_matrix[i], vec3d[i, :])
    return glob_vec3d


def extract_hori_acc(linacc, grav):
#     p = (linacc*grav)*grav/np.linalg.norm(grav, axis=1)[:, None]
    p = (linacc*grav)*grav/(np.linalg.norm(grav, axis=1)*2)[:, None]
    hori_acc = linacc-p
    return hori_acc


def lpf_gaussian(x,times,sigma):
    sigma_k = sigma/(times[1]-times[0]) 
    kernel = np.zeros(int(round(3*sigma_k))*2+1)
    for i in range(kernel.shape[0]):
        kernel[i] =  1.0/np.sqrt(2*np.pi)/sigma_k * np.exp((i - round(3*sigma_k))**2/(- 2*sigma_k**2))

    kernel = kernel / kernel.sum()
    x_long = np.zeros(x.shape[0] + kernel.shape[0])
    x_long[kernel.shape[0]//2 :-kernel.shape[0]//2] = x
    x_long[:kernel.shape[0]//2 ] = x[0]
    x_long[-kernel.shape[0]//2 :] = x[-1]

    x_GC = np.convolve(x_long,kernel,'same')
    return x_GC[kernel.shape[0]//2 :-kernel.shape[0]//2]


def convert_xy_to_rad(X, Y):
    rad = np.array([math.atan2(y,x) for y, x in zip(Y,X)])
    return rad


def convert_rad_to_unit_vector2D(rad):
    unit_vector2D = np.concatenate([
        np.cos(rad)[:, None],
        np.sin(rad)[:, None]
    ], axis=1)
    return unit_vector2D


def diff(x, diff_size=1):
    return np.array([x[i+diff_size]-x[i] for i in range(len(x)-diff_size)])