#%%
import numpy as np
import torch
import quaternion
import os
import h5py
from common import *


#%%
class SequenceDataset(torch.utils.data.Dataset):
    
    def __init__(self, ts_list, hori_acc_lpf_gaussian_list, unit_vector2D_gt_list, window_size, slide_size):
        self.ts_list = ts_list
        self.hori_acc_lpf_gaussian_list = hori_acc_lpf_gaussian_list
        self.unit_vector2D_gt_list = unit_vector2D_gt_list
        self.window_size = window_size
        self.slide_size = slide_size

        self.index_map = []
        for i in range(len(self.unit_vector2D_gt_list)):
            unit_vector2D_gt = self.unit_vector2D_gt_list[i]
            for j in range(0, len(unit_vector2D_gt)-self.window_size, self.slide_size):
                self.index_map.append([i, j])

    def __len__(self):
        return len(self.index_map)
    
    def __getitem__(self, idx):
        seq_id, frame_id = self.index_map[idx][0], self.index_map[idx][1]
        feature = self.hori_acc_lpf_gaussian_list[seq_id][frame_id:frame_id+self.window_size, :].transpose(1, 0)
        target = np.mean(self.unit_vector2D_gt_list[seq_id][frame_id:frame_id+self.window_size], axis=0)
        return feature.astype(np.float32), target.astype(np.float32)


#%%
class GT1205SequenceDataset(SequenceDataset):

    def __init__(self, data_path_list, window_size, slide_size):
        ts_list, hori_acc_lpf_gaussian_list, unit_vector2D_gt_list = [], [], []

        for data_path in data_path_list:
            data = np.loadtxt(data_path, delimiter=',')
            
            # Slice data
            ts, acc, grav, linacc, gyro, mag, quat, pos = self.slice_data(data)

            # Convert to GCS
            glob_linacc = convert_DCS_to_GCS_by_quat(linacc, quat)
            glob_grav = convert_DCS_to_GCS_by_quat(grav, quat)

            # Extract horizontal components
            glob_hori_acc = extract_hori_acc(glob_linacc, glob_grav)

            # Gaussian filter
            hori_acc_x_lpf_gaussian = lpf_gaussian(glob_hori_acc[:, 0], ts, sigma=0.011)
            hori_acc_y_lpf_gaussian = lpf_gaussian(glob_hori_acc[:, 1], ts, sigma=0.011)
            hori_acc_lpf_gaussian = np.concatenate([hori_acc_x_lpf_gaussian[:, None], hori_acc_y_lpf_gaussian[:, None]], axis=1)

            # Ground truth
            vel2d = diff(pos[:, [0,1]], diff_size=10)
            rad = convert_xy_to_rad(vel2d[:, 0], vel2d[:, 1])
            unit_vector2D_gt = convert_rad_to_unit_vector2D(rad)

            ts_list.append(ts)
            hori_acc_lpf_gaussian_list.append(hori_acc_lpf_gaussian)
            unit_vector2D_gt_list.append(unit_vector2D_gt)

        super().__init__(ts_list, hori_acc_lpf_gaussian_list, unit_vector2D_gt_list, window_size, slide_size)

    def slice_data(self, data):
        ts = data[:, 0]
        acc = data[:, 1:4]
        grav = data[:, 4:7]
        gyro = data[:, 7:10]
        mag = data[:, 10:13]
        quat = data[:, 13:17]
        pos = data[:, 17:20]
        linacc = copy.deepcopy(acc-grav)
        return ts, acc, grav, linacc, gyro, mag, quat, pos

#%%
class RoNINSequenceDataset(SequenceDataset):

    def __init__(self, data_path_list, window_size, slide_size):
        ts_list, hori_acc_lpf_gaussian_list, unit_vector2D_gt_list = [], [], []
        
        for data_path in data_path_list:

            self.info = read_json(os.path.join(data_path, 'info.json'))
            ts, linacc, grav, gyro, tango_pos, init_tango_ori = self.load_data(data_path)
            _, ori, _  = self.select_orientation_source(data_path, max_ori_error=20.0, grv_only=True, use_ekf=True)
            
            # Compute the IMU orientation in the Tango coordinate frame.
            ori_q = quaternion.from_float_array(ori)
            rot_imu_to_tango = quaternion.quaternion(*self.info['start_calibration'])
            init_rotor = init_tango_ori * rot_imu_to_tango * ori_q[0].conj()
            ori_q = init_rotor * ori_q

            gyro_q = quaternion.from_float_array(np.concatenate([np.zeros([gyro.shape[0], 1]), gyro], axis=1))
            linacc_q = quaternion.from_float_array(np.concatenate([np.zeros([linacc.shape[0], 1]), linacc], axis=1))
            grav_q = quaternion.from_float_array(np.concatenate([np.zeros([grav.shape[0], 1]), grav], axis=1))

            glob_gyro = quaternion.as_float_array(ori_q * gyro_q * ori_q.conj())[:, 1:]
            glob_linacc = quaternion.as_float_array(ori_q * linacc_q * ori_q.conj())[:, 1:]
            glob_grav = quaternion.as_float_array(ori_q * grav_q * ori_q.conj())[:, 1:]

            # Extract horizontal components
            glob_hori_acc = extract_hori_acc(glob_linacc, glob_grav)

            # Gaussian filter
            hori_acc_x_lpf_gaussian = lpf_gaussian(glob_hori_acc[:, 0], ts, sigma=0.011)
            hori_acc_y_lpf_gaussian = lpf_gaussian(glob_hori_acc[:, 1], ts, sigma=0.011)
            hori_acc_lpf_gaussian = np.concatenate([hori_acc_x_lpf_gaussian[:, None], hori_acc_y_lpf_gaussian[:, None]], axis=1)

            # Ground truth
            vel2d = diff(tango_pos[:, [0,1]], diff_size=10)
            rad = convert_xy_to_rad(vel2d[:, 0], vel2d[:, 1])
            unit_vector2D_gt = convert_rad_to_unit_vector2D(rad)

            # Down sampling and append to list
            ts_list.append(ts[::2])
            hori_acc_lpf_gaussian_list.append(hori_acc_lpf_gaussian[::2, :])
            unit_vector2D_gt_list.append(unit_vector2D_gt[::2, :])

        super().__init__(ts_list, hori_acc_lpf_gaussian_list, unit_vector2D_gt_list, window_size, slide_size)

    def load_data(self, data_path):
        G = 9.806
        with h5py.File(os.path.join(data_path, 'data.hdf5')) as f:
            gyro_uncalib = np.copy(f['synced/gyro_uncalib'])
            linacce = np.copy(f['synced/linacce'])/G
            acce = np.copy(f['synced/acce'])/G
            # grav = f['synced/gravity']
            grav = np.array(acce) - np.array(linacce)
            gyro = gyro_uncalib - np.array(self.info['imu_init_gyro_bias'])
            ts = np.copy(f['synced/time'])
            tango_pos = np.copy(f['pose/tango_pos'])
            init_tango_ori = quaternion.quaternion(*f['pose/tango_ori'][0])
        return ts, linacce, grav, gyro, tango_pos, init_tango_ori

    def select_orientation_source(self, data_path, max_ori_error=20.0, grv_only=True, use_ekf=True):
        ori_names = ['gyro_integration', 'game_rv']
        ori_sources = [None, None, None]

        with open(os.path.join(data_path, 'info.json')) as f:
            info = json.load(f)
            ori_errors = np.array(
                [info['gyro_integration_error'], info['grv_ori_error'], info['ekf_ori_error']])
            init_gyro_bias = np.array(info['imu_init_gyro_bias'])

        with h5py.File(os.path.join(data_path, 'data.hdf5')) as f:
            ori_sources[1] = np.copy(f['synced/game_rv'])
            if grv_only or ori_errors[1] < max_ori_error:
                min_id = 1
            else:
                if use_ekf:
                    ori_names.append('ekf')
                    ori_sources[2] = np.copy(f['pose/ekf_ori'])
                min_id = np.argmin(ori_errors[:len(ori_names)])
                # Only do gyro integration when necessary.
                if min_id == 0:
                    ts = f['synced/time']
                    gyro = f['synced/gyro_uncalib'] - init_gyro_bias
                    ori_sources[0] = gyro_integration(ts, gyro, ori_sources[1][0])

        return ori_names[min_id], ori_sources[min_id], ori_errors[min_id]

