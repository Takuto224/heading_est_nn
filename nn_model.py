import numpy as np
import copy
import os
import time
from joblib import Parallel, delayed
import matplotlib.pyplot as plt
import math

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch import optim
from torch.nn import TransformerEncoder, TransformerEncoderLayer
    
class SimpleCNN(nn.Module):
    
    # define network params by using constructor
    def __init__(self, window, k1, k2, s1, s2, out_ch1, out_ch2, hidden_size2):
        
        # super class constructor
        super(SimpleCNN, self).__init__()
        
        self.input_size = window
        self.output_size = 2
        
        self.k1 = k1
        self.s1 = s1
        self.in_ch1 = 2
        self.out_ch1 = out_ch1
        
        self.k2 = k2
        self.s2 = s2
        self.in_ch2 = out_ch1
        self.out_ch2 = out_ch2

        self.hidden_size1 = int((( int((self.input_size - self.k1) / self.s1 + 1) - self.k2) / self.s2 + 1) * self.out_ch2)
        self.hidden_size2 = hidden_size2
        
        self.conv1 = nn.Conv1d(self.in_ch1, self.out_ch1, self.k1, self.s1)
        self.conv2 = nn.Conv1d(self.in_ch2, self.out_ch2, self.k2, self.s2)
        
        self.bn1 = nn.BatchNorm1d(self.out_ch1)
        self.bn2 = nn.BatchNorm1d(self.out_ch2)
        
        self.linear1 = nn.Linear(self.hidden_size1, self.hidden_size2)
        self.linear2 = nn.Linear(self.hidden_size2, self.output_size)

        
    def forward(self, features):
        
        x= self.conv1(features)
        x = self.bn1(x)
        x = F.relu(x)
        x= self.conv2(x)
        x = self.bn2(x)
        x = F.relu(x)
        x = torch.flatten(x, start_dim=1)
        x = self.linear1(x)
        x = F.relu(x)
        x = self.linear2(x)
        output = x/torch.norm(x, dim=1, keepdim=True)
        
        return output
    
    
class SimpleCNN2d(nn.Module):
    
    # define network params by using constructor
    def __init__(self, window, k1, k2, s1, s2, out_ch1, out_ch2, hidden_size2):
        
        # super class constructor
        super(SimpleCNN2d, self).__init__()
        
        self.input_size = window
        self.output_size = 2
        
        self.k1 = k1
        self.s1 = s1
        self.in_ch1 = 1
        self.out_ch1 = out_ch1
        
        self.k2 = k2
        self.s2 = s2
        self.in_ch2 = out_ch1
        self.out_ch2 = out_ch2

        self.hidden_size1 = int((( int((self.input_size - self.k1) / self.s1 + 1) - self.k2) / self.s2 + 1) * self.out_ch2)
        self.hidden_size2 = hidden_size2*2
        
        self.conv1 = nn.Conv2d(self.in_ch1, self.out_ch1, self.k1, self.s1)
        self.conv2 = nn.Conv2d(self.in_ch2, self.out_ch2, self.k2, self.s2)
        
        self.bn1 = nn.BatchNorm2d(self.out_ch1)
        self.bn2 = nn.BatchNorm2d(self.out_ch2)
        
        self.linear1 = nn.Linear(self.hidden_size1, self.hidden_size2)
        self.linear2 = nn.Linear(self.hidden_size2, self.output_size)

        
    def forward(self, features):
        x = torch.unsqueeze(features, 1).transpose(2, 3)
        print(x.size())
        x = self.conv1(x)
        x = self.bn1(x)
        x = F.relu(x)
        x = self.conv2(x)
        x = self.bn2(x)
        x = F.relu(x)
        x = torch.flatten(x, start_dim=1)
        print(x.size())
        x = self.linear1(x)
        x = F.relu(x)
        x = self.linear2(x)
        output = x/torch.norm(x, dim=1, keepdim=True)
        
        return output
    

class SimpleLSTM(nn.Module):
    
    # define network params by using constructor
    def __init__(self, input_size, output_size, hidden_size, num_layers, dropout):
        
        # super class constructor
        super(SimpleLSTM, self).__init__()
        
        self.input_size = input_size
        self.output_size = 2
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.dropout = dropout
        
        if self.num_layers == 1:
            self.lstm = nn.LSTM(input_size=self.input_size, hidden_size=self.hidden_size, num_layers=self.num_layers, bidirectional=True)
        else:
            self.lstm = nn.LSTM(input_size=self.input_size, hidden_size=self.hidden_size, num_layers=self.num_layers, dropout=self.dropout, bidirectional=True)
        self.linear = nn.Linear(self.hidden_size*2, self.output_size)
        self.bn = nn.BatchNorm1d(self.hidden_size*2)

        
    def forward(self, inputs):
        x = inputs.transpose(1, 2).transpose(0, 1).contiguous()
        # DropoutがBNの代わりだとするとCNNと対応する？
        x, _ = self.lstm(x)
        x = torch.cat((x[-1, :, :self.hidden_size], x[0, :, self.hidden_size:]),dim=1)
#         x = self.bn(x) 
        x = F.relu(x)
        x = self.linear(x)
        output = x/torch.norm(x, dim=1, keepdim=True)
        
        return output


class DualCNNLSTM(nn.Module):
    def __init__(self, channel, kernel_size, hidden_size):
        super(DualCNNLSTM, self).__init__()
        self.dim_in = 2
        self.channel = channel
        self.kernel_size = kernel_size
        self.hiddensize = hidden_size
        self.layersize = 1
        self.batchsize = 512

        # for convLSTM imu_input
        self.convl1 = nn.Conv1d(1, self.channel, self.kernel_size)
        self.convl1_drop = nn.Dropout(p=0.5)
        self.convl21 = nn.Conv1d(self.channel, self.channel, self.kernel_size)
        self.convl22 = nn.Conv1d(self.channel, self.channel, self.kernel_size)
        self.convl31 = nn.Conv1d(self.channel, self.channel, 2 * self.kernel_size)
        self.convl32 = nn.Conv1d(self.channel, self.channel, 2 * self.kernel_size)
        self.convl21_drop = nn.Dropout(p=0.5)
        self.convl22_drop = nn.Dropout(p=0.5)
        self.convl31_drop = nn.Dropout(p=0.5)
        self.convl32_drop = nn.Dropout(p=0.5)

        # for lstm mixed feature
        self.mlstm = nn.LSTM(2 * self.dim_in * self.channel, self.hiddensize, self.layersize)

        self.output_vel = nn.Sequential(
            nn.Linear(self.hiddensize, 2 * self.hiddensize),
            nn.BatchNorm1d(2 * self.hiddensize),
            nn.ReLU(),
            nn.Linear(2 * self.hiddensize, 2),
#             nn.ReLU()
        )

    def forward(self, features):
        # conv lstm
#         clstm_input = features.transpose(1, 2).contiguous()
#         print(features.size())
        clstm_input = features
        clstm_input = torch.split(clstm_input, 1, dim=1)
        mid1 = []
        mid2 = []
        for axis in clstm_input:
            axis_mid = F.relu(self.convl1_drop(self.convl1(axis)))
            axis_out1 = F.relu(self.convl21_drop(self.convl21(axis_mid)))
            axis_out1 = F.relu(self.convl22_drop(self.convl22(axis_out1)))
            mid1.append(axis_out1)
            axis_out2 = F.relu(self.convl31_drop(self.convl31(axis_mid)))
            axis_out2 = F.relu(self.convl32_drop(self.convl32(axis_out2)))
            mid2.append(axis_out2)

        mid1 = torch.cat(mid1, dim=1).transpose(1, 2).transpose(0, 1).contiguous()
        mid2 = torch.cat(mid2, dim=1).transpose(1, 2).transpose(0, 1).contiguous()

        # lstm after mix
        mlstm_input = torch.cat([mid1[-1 * mid2.size(0):, :, :], mid2], dim=2)
        mlstm_out, _ = self.mlstm(mlstm_input)
        mlstm_out = mlstm_out.transpose(0, 1).contiguous()

        # print(conv_outs.size(), mlstm_out.size())
        mix = mlstm_out[:, -1, :]
        output = self.output_vel(mix)

        return output
    
    
class TransformerModel(nn.Module):

    def __init__(self, d_model: int, nhead: int, d_hid: int, nlayers: int, dropout: float = 0.5):
        '''
        d_model: 2
        nhead: 1
        d_hid: 32
        n_layers: 2
        dropout: 0.5
        '''
        super().__init__()
        self.model_type = 'Transformer'
        self.pos_encoder = PositionalEncoding(d_model, dropout)
        encoder_layers = TransformerEncoderLayer(d_model, nhead, d_hid, dropout)
        self.transformer_encoder = TransformerEncoder(encoder_layers, nlayers)
        self.d_model = d_model
        self.decoder1 = nn.Linear(d_model*400, 64)
        self.decoder2 = nn.Linear(64, 2)

        self.init_weights()

    def init_weights(self) -> None:
        initrange = 0.1
#         self.encoder.weight.data.uniform_(-initrange, initrange)
        self.decoder1.bias.data.zero_()
        self.decoder1.weight.data.uniform_(-initrange, initrange)
        self.decoder2.bias.data.zero_()
        self.decoder2.weight.data.uniform_(-initrange, initrange)

    def forward(self, src):
        """
        Args:
            src: Tensor, shape [seq_len, batch_size]
            src_mask: Tensor, shape [seq_len, seq_len]

        Returns:
            output Tensor of shape [seq_len, batch_size, ntoken]
        """
        src = src.transpose(0, 1).transpose(0, 2).contiguous()
        src = self.pos_encoder(src)
#         print(src.size())
        output = self.transformer_encoder(src)
#         print(output.size())
        output = torch.flatten(output.transpose(0, 1).contiguous(), 1, 2)
#         print(output.size())
        output = self.decoder1(output)
        output = self.decoder2(output)
        return output
    
    
class PositionalEncoding(nn.Module):

    def __init__(self, d_model: int, dropout: float = 0.1, max_len: int = 5000):
        super().__init__()
        self.dropout = nn.Dropout(p=dropout)

        position = torch.arange(max_len).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2) * (-math.log(10000.0) / d_model))
        pe = torch.zeros(max_len, 1, d_model)
        pe[:, 0, 0::2] = torch.sin(position * div_term)
        pe[:, 0, 1::2] = torch.cos(position * div_term)
        self.register_buffer('pe', pe)

    def forward(self, x):
        """
        Args:
            x: Tensor, shape [seq_len, batch_size, embedding_dim]
        """
        x = x + self.pe[:x.size(0)]
        return self.dropout(x)