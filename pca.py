import numpy as np
from sklearn.decomposition import PCA
from common import convert_xy_to_rad, convert_rad_to_unit_vector2D


class PCAPredictor():
    
    def __init__(self, window, slide):
        self.window= window
        self.slide = slide
        
    def fit(self, hori_acc):
        eigenvector1 = []
        pca = PCA(n_components=2)

        for i in range(0, len(hori_acc)-self.window, self.slide):
            pca.fit(hori_acc[i:i+self.window, :])
            eigenvector1.append(pca.components_[0])
        eigenvector1 = np.array(eigenvector1)

        rad = convert_xy_to_rad(eigenvector1[:, 0], eigenvector1[:, 1])
        self.rad = rad
        self.unit_vector2D = convert_rad_to_unit_vector2D(self.rad)

    def get_rad(self):
        return self.rad
    
    def get_unit_vector2D(self):
        return self.unit_vector2D