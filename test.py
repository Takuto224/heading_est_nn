import torch
from torch import optim, nn
from torch.utils.data import DataLoader
import time
import os

from nn_model import SimpleCNN
from data import *


def load_data_path_list(list_path):
    data_path_list = []
    with open(list_path, 'r') as f:
        lines = f.readlines()
        for line in lines:
            if line[0] != '#':
                data_path_list.append(line.rstrip('\n'))
    return data_path_list


def main(args):

    if args.dataset == 'gt1205':
        XSequenceDataset = GT1205SequenceDataset
        save_path = './pretrained_model/gt1205'
    elif args.dataset == 'ronin':
        XSequenceDataset = RoNINSequenceDataset
        save_path = './pretrained_model/ronin'
    else:
        print('Not found')

    test_data_path_list = load_data_path_list(args.test_list)

    # prepare for test
    gpuid = int(args.gpuid)
    device = 'cuda:' + str(gpuid) if torch.cuda.is_available() else 'cpu'

    model = SimpleCNN(
        window=400, 
        k1=9, 
        k2=9, 
        s1=1, 
        s2=1, 
        out_ch1=8, 
        out_ch2=16, 
        hidden_size2=32).to(device)

    save_data = torch.load(os.path.join(save_path, 'model.pt'))
    model.load_state_dict(save_data['model_state_dict'])

    for test_data_path in test_data_path_list:
        # Load data
        test_dataset = XSequenceDataset(
            [test_data_path], 
            window_size=400,
            slide_size=1)


        batch_test = torch.utils.data.DataLoader(
            test_dataset,
            batch_size=1024,
            shuffle=False,
            num_workers=4)

        ts = test_dataset.ts_list[0]

        # test
        model.eval()
        pred_all, targ_all = [], []
        with torch.no_grad():
            for feat, targ in batch_test:

                feat, targ = feat.to(device), targ.to(device)
                pred = model(feat)
                pred_all.append(pred.cpu().detach().numpy())
                targ_all.append(targ.cpu().detach().numpy())

        pred_all = np.concatenate(pred_all, axis=0)
        targ_all = np.concatenate(targ_all, axis=0)
        
        if not os.path.exists(os.path.join(save_path, 'predict')):
            os.makedirs(os.path.join(save_path, 'predict'))

        np.savetxt(
            os.path.join(save_path, 'predict', os.path.basename(test_data_path)),
            np.concatenate([ts[:len(pred_all), None], pred_all, targ_all], axis=1), 
            fmt=['%.3f', '%.5f', '%.5f', '%.5f', '%.5f'], delimiter=',')


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset', type=str)
    parser.add_argument('--gpuid', type=int)
    parser.add_argument('--test_list', type=str)

    args = parser.parse_args()

    main(args)