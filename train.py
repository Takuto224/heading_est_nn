#%%
import torch
from torch import optim, nn
from torch.utils.data import DataLoader
import os
import time

from nn_model import SimpleCNN
from data import *
from common import *

#%%
def load_data_path_list(list_path):
    data_path_list = []
    with open(list_path, 'r') as f:
        lines = f.readlines()
        for line in lines:
            if line[0] != '#':
                data_path_list.append(line.rstrip('\n'))
    return data_path_list


#%%
def main(args):

    if args.dataset == 'gt1205':
        XSequenceDataset = GT1205SequenceDataset
        save_path = './pretrained_model/gt1205'
    elif args.dataset == 'ronin':
        XSequenceDataset = RoNINSequenceDataset
        save_path = './pretrained_model/ronin'
    else:
        print('Not found')
    
    if not os.path.exists(save_path):
        os.makedirs(save_path)

    train_data_path_list = load_data_path_list(args.train_list)
    val_data_path_list = load_data_path_list(args.val_list)

    # Load data
    train_dataset = XSequenceDataset(
        train_data_path_list, 
        window_size=400, 
        slide_size=10)
    val_dataset = XSequenceDataset(
        val_data_path_list, 
        window_size=400, 
        slide_size=10)

    batch_train = torch.utils.data.DataLoader(
        train_dataset, 
        batch_size=512, 
        shuffle=True, 
        num_workers=4)
    batch_val = torch.utils.data.DataLoader(
        val_dataset, 
        batch_size=512, 
        shuffle=True, 
        num_workers=4)

    # prepare for training
    gpuid = int(args.gpuid)
    device = 'cuda:' + str(gpuid) if torch.cuda.is_available() else 'cpu'

    model = SimpleCNN(
        window=400, 
        k1=9, 
        k2=9, 
        s1=1, 
        s2=1, 
        out_ch1=8, 
        out_ch2=16, 
        hidden_size2=32).to(device)

    criterion = nn.MSELoss()
    optimizer = optim.Adam(model.parameters(), lr=0.001)

    train_loss_list, val_loss_list = [], []
    best_val_loss = np.inf
    epochs = 500

    try:
        for epoch in range(1, epochs+1):

            epoch_start_time = time.time()

            # train
            model.train()
            epoch_loss_list = []
            for feat, targ in batch_train:

                feat, targ = feat.to(device), targ.to(device)
                pred = model(feat)
                loss = criterion(pred, targ)

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
                
                epoch_loss_list.append(loss.item())

            avg_train_loss = np.mean(epoch_loss_list)
            train_loss_list.append(avg_train_loss)

            # val
            model.eval()
            epoch_loss_list = []
            with torch.no_grad():
                for feat, targ in batch_val:

                    feat, targ = feat.to(device), targ.to(device)
                    pred = model(feat)
                    loss = criterion(pred, targ)

                    epoch_loss_list.append(loss.item())

            avg_val_loss = np.mean(epoch_loss_list)
            val_loss_list.append(avg_val_loss)

            if avg_val_loss < best_val_loss:
                torch.save({
                    'model_state_dict': model.state_dict(),
                    'epoch': epoch,
                    'optimizer_state_dict': optimizer.state_dict()
                    }, os.path.join(save_path, 'model.pt'))

            print('='*50)
            print('train: {:2.4f} | val: {:2.4f} | {}/{} | {:2.4f}sec'.format(avg_train_loss, avg_val_loss, epoch, epochs, time.time() - epoch_start_time))

        loss_log = {
            'train_loss': train_loss_list,
            'val_loss': val_loss_list
        }

        save_json(os.path.join(save_path, 'loss.json'), loss_log)
        
    except KeyboardInterrupt:
        print('Exiting from training early')


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset', type=str)
    parser.add_argument('--gpuid', type=int)
    parser.add_argument('--train_list', type=str)
    parser.add_argument('--val_list', type=str, default=None)

    args = parser.parse_args()

    main(args)
# %%
